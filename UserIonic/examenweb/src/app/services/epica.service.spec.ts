import { TestBed } from '@angular/core/testing';

import { EpicaService } from './epica.service';

describe('EpicaService', () => {
  let service: EpicaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EpicaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
