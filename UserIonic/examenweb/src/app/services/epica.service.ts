import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  Solicitud } from  '../folder/interfaces/solicitud';

@Injectable({
  providedIn: 'root'
})
export class EpicaService {

  apiURL='https://basedatosexamen-535c9.firebaseio.com/solicitud'

  constructor(private clienteService:HttpClient) { }

  public getSolicitud(soli='')
  {
    if(soli=='')
    return this.clienteService.get(`${this.apiURL}.json`).toPromise()
    return this.clienteService.get(`${this.apiURL}/${soli}.json`).toPromise()
  }

  public postSolicitud( solicituds:Solicitud)
  {
    return this.clienteService.put(`${this.apiURL}/${solicituds.ci}.json`,
    solicituds,{  headers :{'Content-Type':'application/json'}}).toPromise();
  }

  public deleteSolicitud(cedu:string)
  {
    return this.clienteService.delete(`${this.apiURL}/${cedu}.json`).toPromise();
  }


}
