import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EpicaPage } from './epica.page';

describe('EpicaPage', () => {
  let component: EpicaPage;
  let fixture: ComponentFixture<EpicaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpicaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EpicaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
