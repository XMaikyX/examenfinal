import { Component, OnInit } from '@angular/core';
import { Solicitud } from 'src/app/folder/interfaces/solicitud';
import { ICrud } from 'src/app/folder/interfaces/ICrud'; 
import { EpicaService } from 'src/app/services/epica.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-epica',
  templateUrl: './epica.page.html',
  styleUrls: ['./epica.page.scss'],
})
export class EpicaPage implements OnInit, ICrud {

  public soliClient: Solicitud={ci:'', nombre:'', estado:'Iniciado',tiempo:'Tiempo-Indefinido',precio:400,requerimientos:'ninguno asignado' }
  public soliClients: Solicitud[]=[];


  constructor( private epica:EpicaService, private toast:ToastController) { }

  async mostrarMensaje(notificacion:string, duracion:number){
    const notificacions=await this.toast.create({message:notificacion, duration:duracion})

    notificacions.present();
  }


  nuevo()
  {
    this.soliClient={ci:'',nombre:'',estado:'Iniciado',tiempo:'Tiempo-Indefinido',precio:400,requerimientos:''};
  }
  guardar(): void {
    this.epica.postSolicitud(this.soliClient).then(resp=>{
      
      this.mostrarMensaje('Cita Guardada Correctamente',2000)
    })
    .catch(error=>
      this.mostrarMensaje('No se pudo Guardar',200)
      )
  }

  consultar(): void {
    this.epica.getSolicitud().then(respuesta=>{
      this.soliClients=[];
      for(let elemento in respuesta)
      {
        this.soliClients.push(respuesta[elemento]);
      }
      
    })
    .catch(error=>{
      console.log(error)
    })
  }

  eliminar(): void {
    this.epica.deleteSolicitud(this.soliClient.ci).then(resp=>{
      console.log('se elimino correctamente')  
    })
    .catch(error=>{
      console.log('no se pudo eliminar')
    })
  }

  ngOnInit() {
  }

  parametros(cedu:string)
  {
    this.epica.getSolicitud(cedu).then(resp=>{
      this.soliClient=<Solicitud>resp;
    })
    .catch(error=>{
      console.log(error )
    })
  }
}
