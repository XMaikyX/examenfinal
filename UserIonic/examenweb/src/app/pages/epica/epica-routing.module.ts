import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EpicaPage } from './epica.page';

const routes: Routes = [
  {
    path: '',
    component: EpicaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EpicaPageRoutingModule {}
