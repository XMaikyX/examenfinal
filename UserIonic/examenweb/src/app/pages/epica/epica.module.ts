import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EpicaPageRoutingModule } from './epica-routing.module';

import { EpicaPage } from './epica.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EpicaPageRoutingModule
  ],
  declarations: [EpicaPage]
})
export class EpicaPageModule {}
