export interface ICrud
{
    nuevo():void;
    guardar():void;
    consultar():void;
    eliminar():void;
}