window.addEventListener('load',function(){
    btnnuevo.addEventListener('click',function(){
        txtestados.value="";
        txtcedula.value="";
        txtnombre.value="";
        txtprecio.value="";
        txtrequerimiento.value="";
        txttiempo.value="";
    })
    btnconsultar.addEventListener('click',function(){
        let url= `https://basedatosexamen-535c9.firebaseio.com/solicitud.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            console.log(Object.entries(resultado2))
            let tabla= "<table border=1 class=contenido>";
            {
                tabla+="<tr>"
                tabla+=`<td>Estado</td> <td>Cedula</td> <td>Nombre</td> <td>Precio</td> <td>Tiempo</td> <td>Requerimientos</td>`
                tabla+="</tr>" 
            }
            for(let elemento in resultado2)
            {
                tabla+="<tr>"
                tabla+=`<td> ${resultado2[elemento].estado}</td> <td> <button class='boton'> ${resultado2[elemento].ci}</button></td> <td> ${resultado2[elemento].nombre}</td> <td>${resultado2[elemento].precio}</td> <td> ${resultado2[elemento].tiempo}</td> <td> ${resultado2[elemento].requerimientos}</td>`
                tabla+="</tr>"
            }
            tabla+= "</table>"
            requerimiento.innerHTML=tabla

            document.querySelectorAll('.boton').forEach(elemento=>{
                elemento.addEventListener('click',function(){
                    let url1= `https://basedatosexamen-535c9.firebaseio.com/solicitud/${elemento.innerHTML.trim()}.json`
                    console.log(url1)

                    fetch(url1).then(valores=>{return valores.json()}).then(valores2=>{
                        txtestados.value=valores2.estado;
                        txtcedula.value=valores2.ci;   
                        txtnombre.value=valores2.nombre;
                        txttiempo.value=valores2.tiempo;
                        txtprecio.value=valores2.precio;
                        txtrequerimiento.value=valores2.requerimientos;
                    } )
                })
            })
        })
        .catch(error=>{
            console.log(error)
        })
    })
    btngrabar.addEventListener('click',function(){
        let url= `https://basedatosexamen-535c9.firebaseio.com/solicitud/${txtcedula.value}.json`
        let cuerpo={ci:txtcedula.value,nombre:txtnombre.value, estado: txtestados.value,precio:txtprecio.value, 
            requerimientos: txtrequerimiento.value,tiempo:txttiempo.value};
        
        
        fetch(url,{
            method:'PUT',
            body: JSON.stringify(cuerpo),
            headers:{
                'Content-Type':'application/json'
            }
        }).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            alert("se ha Registrado")
            console.log(resultado2.nombre)
        })
        .catch(error=>{
            console.log('No se ha añadido',error)
        })
    })
    btneliminar.addEventListener('click',function(){
        let url= `https://basedatosexamen-535c9.firebaseio.com/solicitud/${txtcedula.value}.json`    
        
        fetch(url,{
            method:'DELETE'
        }).then(resultado=>{
            alert("se ha Eliminado correctamente")
            return resultado.json();
        })
        .then(resultado2=>{
            
            console.log(resultado2.nombre)
        })
        .catch(error=>{
            console.log('No se ha Eliminado',error)
        })
    })

})